package com.kartal.java13features;

public class TextBlocksSample {

    public static void showWhiteSpace(String string) {
        System.out.println(string.replaceAll(" ", ".").replaceAll("\n", "\\\\n\n"));
    }

    public static void Sample() {
        String text = """
                This is "definitely"
                a multi-line
                String
                """;
        showWhiteSpace(text);
    }
}
