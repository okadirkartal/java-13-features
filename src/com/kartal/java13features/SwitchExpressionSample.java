package com.kartal.java13features;

public class SwitchExpressionSample {

    //SWITCH EXPRESSION SAMPLE
    public String SwitchExpressionSample(int monthNumber) {
        String monthName = switch (monthNumber) {
            case 1 -> "January";
            case 2 -> "February";

            //rest of cases omitted

            default -> "Unknown";
        };
        return monthName;
    }

    public String SwitchExpressionSampleWithYield(int monthNumber) {
        String monthName = switch (monthNumber) {
            case 1 -> {
                String month = "January";
                yield month;
            }
            case 2 -> {
                String month = "February";
                yield month;
            }

            //rest of cases

            default -> "Unknown";
        };
        return monthName;
    }
}
